<?php include('geheim.php'); ?>

<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src = "Uhrzeit.js"></script>
</head>

<header>
<h2>I-System Türklingel V1 Team 24 / Impressum</h2>
<?php $timestamp = time();
$datum = date("d.m.Y", $timestamp);
echo $datum;
?>
<p id = "uhr"></p>
<hr/>
</header>

<body>

<div id="column1">
<a href="Steuerungsmodul.php">
Steuerungs_Modul
</a>
</div><br/>

<div id="column2">
<a href="Einstellungen.php">
Einstellungen
</a>
</div><br/>

<div id="column3">
<a href="Loghistorie.php">
Log_Historie
</a>
</div><br/>

<hr>

<div id="inhalt">
<table>
<tr>
<td>IiSystem2017</td>
<td>Version 1.0</td>
</tr>
<tr>
<td>Firma</td>
<td>ET16A  Team 24 DHBW Mosbach</td>
</tr>
<tr>
<td>Adresse</td>
<td>Lohrtalweg 10</td>
</tr>
<tr>
<td>PLZ,Ort</td>
<td>74821 Mosbach</td>
</tr>
<tr>
<td>Anprechpartner: </td>
<td>
Jochen Streng</br>
Fabian Müller</br>
Jan Grapengeter</br>
</td>
</tr>
</table>
</div>

<hr>

<div id="impressum">
<a href="Impressum.php">
Impressum
</a>
</div>
<br/>
<div id="Log_Out">
<a href="logout.php">
Log_Out
</a>
</div>

</body>
</html>
