<?php include('geheim.php');
// Einbinden der Konfigurationsdatei für mysql
require_once('/var/www/Datenbankverbindung.php');
?>
<!DOCTYPE html>
<html>
<head>
	<!--einbinden von Bootstrap-->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
 <!-- einbinden von Uhrzeit.js um aktuelle Uhrzeit anzuzeigen-->
<script src = "Uhrzeit.js"></script>
 <!--aktuallisiert alle 5 Sekunden die Seite-->
<meta http-equiv="refresh" content="5; URL=/Steuerungsmodul.php">

<title>
	Steuerungsmodul
</title>
<header>

	<h2>Türklingel V1 Team 24 / Steuerungsmodul</h2>
<!--gibt das aktuelle Datum auf der Seite aus-->
	<?php $timestamp = time();
	$datum = date("d.m.Y", $timestamp);
	echo $datum;
	?>
	<!--git die aktuelle Uhrzeit auf der Seite aus-->
	<p id = "uhr"></p>

<hr/>
</header>
</head>

<body>
<!--Zur Seite Einstellungen -->
<div id="column2">
<a href="Einstellungen.php">
Einstellungen
</a>
</div></br>

<!--Zur Seite LOG_History-->
<div id="column3">
<a href="Loghistorie.php">
Log_Historie
</a>
</div></br>



<!--Systemzustand ändern(Aus, Automatik, Manuell) -->
<div id="inhalt">

<td>Systemzustand: </td>

<?php

		$abfrage = "select * from Systemzustand";
		//überprüfen ob abfrage korrekt
		if(!$result = $db_link->query($abfrage))
		{
			echo 'fehler';
		}

		//Speichern der Datenbankeinträge
		while($row = $result->fetch_assoc())
		{
			$Systemzustand = $row['Zustand'];
		}

		//Abfrage des Systemzustandes
		//Entsprechende ausgabe auf Weboberfläche
		switch($Systemzustand)
		{
			case 0:
				echo "Aus";
				break;
			case 1:
				echo "Automatik";
				break;
			case 2:
				echo "Manuell";
				break;
			default:
				echo "Fehler";
				break;
		}


?>
</br>

</td>
</tr>
<!--Türzustand anzeigen-->
<tr>
<td>Türzustand: </td>
<td>
<?php

		// Datenbankanfrage, zeigt ob Tür geschlossen oder offen ist
		$abfrage = "select * from Tuerzustand";

		//Ausgabe wenn Fehler
		if(!$result = $db_link->query($abfrage))

		{
			echo 'fehler';
		}

		$Tuerzustand = $result->fetch_assoc();

		// Abfragge welcher Zustand gewählt ist
		// Entsprechende Ausgabe auf Weboberfläche

		if($Tuerzustand['Zustand'] == 0)
		{
			echo "Tür geschlossen";
		}
		else
		{
			echo "Tür offen";
		}


?>
</td>
</tr>
</br>

<tr>
<td>Türöffner: </td>
<form method = "POST">
<td><button type = "submit" name = "aktion" value ="1">öffnen</button></td>
</form>
<?php
		//Wenn öffnen gedrückt wird, wird in Datenbank Wert 1 gespeichert,
		//C++ Programm muss dies auswerten (Tür öffnen) und wert in datenbank auf 0 setzten;
		if ( $_POST['aktion'] == 1)
		{
				$Datenbankeintrag = "update Tuerverhalten set Nummer = 2 where Nummer = 1";
				$Datenbankeintrag = mysqli_query($db_link,$Datenbankeintrag);
		}
?>

</tr>
</table>
<br/>
</div>

<!--Ansage anzeigen-->
<tr>
<td>Ansage:</td>
<td>
<?php

		// Datenbankanfrage, welche Ansage gewählt wurde
		$abfrage = "select * from Ansage where Nummer = 1";

		//Ausgabe wenn Fehler
		if(!$result = $db_link->query($abfrage))
		{
			echo 'fehler';
		}

		while($row = $result->fetch_assoc())
		{
			$Ansage = $row['Ansage'];
		}

		// Abfrage welche Ansage gewählt ist
		// Entsprechende Ausgabe auf Weboberfläche

		switch ($Ansage){
			case 0:
				echo "1";
				break;
			case 1:
				echo "2";
				break;
			case 2:
				echo"3";
				break;
			case 3:
				echo "Zufall";
				break;
			default:
				echo "Fehler";
				break;
		}
?>
</td>
</tr>




<!--Zur Seite Impressum-->
<div id="impressum">
<a href="Impressum.php">
Impressum
</a>
</div>
<br/>

<!--Ausloggen-->
<div id="Log_Out">
<a href="logout.php">
Log_Out
</a>
</div>

</body>
</html>
