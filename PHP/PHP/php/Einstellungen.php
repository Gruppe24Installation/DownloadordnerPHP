
<?php include('geheim.php');
// Einbinden der Konfigurationsdatei für mysql
require_once('/var/www/Datenbankverbindung.php'); ?>
<!DOCTYPE html>
<html>
<head>
<!--Seite wird alle 3 Sekunden aktualisiert um Systemzustand anzuzeigen-->
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<!--Einbinden von Bootstrap-->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<!Einbinden von Uhrzeit.js um aktuelle Uhrzeit anzuzeigen-->
<script src = "Uhrzeit.js"></script>
<header>
<h2>Türklingel V1 Team 24 / Einstellungen</h2>
<?php

//Aktuelles Datum und Uhrzeit anzeigen
$timestamp = time();
$datum = date("d.m.Y", $timestamp);
echo $datum;
?>
<p id = "uhr"></p>
<hr/>
</header>
</head>

<body>

<!--Öffnen der Seite Steuerungs_Modul-->
<div id="column1">
<a href="Steuerungsmodul.php">
Steuerungs_Modul
</a>
</div><br/>

<!--Öffnen der Seite Einstellungen-->
<div id="column2">
<a href="Einstellungen.php">
Einstellungen
</a>
</div><br/>

<!--Öffnen der Seite Log_Historie-->
<div id="column3">
<a href="Loghistorie.php">
Log_Historie
</a>
</div><br/>

<!--Öffnen der Seite EmailAdressen-->
<td> <a href= "email.php">Emailadressen</a>
</td>
</tr>

<hr>

<div id="inhalt">
<table>





<!--Tür Begrüßungsansage ändern-->
<tr>
<td>Ansage ändern</td>
<td>
<form method = "POST" >
<select name = "Ansage">
	<option value="<?php echo $_POST['Ansage'];?>"> </option>
	<option value = "0">1</option>
	<option value = "1">2</option>
	<option value = "2">3</option>
	<option value = "3">Zufall</option>
</select>
<input type = "submit">
</form>
</td>
<?php
//Ausgewählter Systemzustand wird in Datenbank Systemzustand geschrieben
switch($_POST['Ansage']){
	case 0:
		$Datenbankeintrag = "update Ansage set Ansage = 0 where Nummer = 1";
		$Datenbankeintrag = mysqli_query($db_link,$Datenbankeintrag);
		break;
	case 1:
		$Datenbankeintrag = "update Ansage set Ansage = 1 where Nummer = 1";
		$Datenbankeintrag = mysqli_query($db_link,$Datenbankeintrag);
		break;
	case 2:
		$Datenbankeintrag = "update Ansage set Ansage = 2 where Nummer = 1";
		$Datenbankeintrag = mysqli_query($db_link,$Datenbankeintrag);
		break;
	case 3:
		$zufall = rand(0,3);
		$Datenbankeintrag = "update Ansage set Ansage = '$zufall' where Nummer = 1";
		$Datenbankeintrag = mysqli_query($db_link,$Datenbankeintrag);
		break;

	default:
		echo "fehler";
		break;
}
?>


<!--Systemzustand ändern(Aus, Automatik, Manuell) -->
<div id="inhalt">
<tr>
<td>Systemzustand</td>
<td>
<form method = "POST" >
<select name = "zustand">
	<option value="<?php echo $_POST['zustand'];?>"> </option>
	<option value = "0">Aus</option>
	<option value = "1">Automatik</option>
	<option value = "2">Manuell</option>
</select>
<input type = "submit">
</form>
</td>
<?php
//Ausgewählter Systemzustand wird in Datenbank Systemzustand geschrieben
switch($_POST['zustand']){
	case 0:
		$Datenbankeintrag = "update Systemzustand set Zustand = 0";
		$Datenbankeintrag = mysqli_query($db_link,$Datenbankeintrag);
		break;
	case 1:
		$Datenbankeintrag = "update Systemzustand set Zustand = 1";
		$Datenbankeintrag = mysqli_query($db_link,$Datenbankeintrag);
		break;
	case 2:
		$Datenbankeintrag = "update Systemzustand set Zustand = 2";
		$Datenbankeintrag = mysqli_query($db_link,$Datenbankeintrag);
		break;
	default:
		echo "fehler";
		break;
}
?>
</table>
<br/>

<!--
<!--Bei Betätigung wird ein Beispielsound abgespielt
<a onclick='document.getElementsByTagName("audio")[0].play();return false' href="#">klick mich f&uuml;r einen Sound</a>
<audio>
<source src="sound.mp3">
<source src="sound.ogg">
</audio>
-->

</div>

<!--Öffnen der Seite impressum-->
<hr>
<div id="impressum">
<a href="Impressum.php">
Impressum
</a>
</div>
<br/>

<!--Website verlasse-->
<div id="Log_Out">
<a href="logout.php">
Log_Out
</a>
</div>



</body>
</html>
