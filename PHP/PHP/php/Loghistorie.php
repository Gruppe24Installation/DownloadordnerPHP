<?php require_once('geheim.php');?>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src = "Uhrzeit.js"></script>
<meta http-equiv="refresh" content="5; URL=/Loghistorie.php">
<title>
	Loghistorie
</title>
</head>

<header>

<h2>Türklingel V1 Team 24 / Loghistorie</h2>

	<?php
	$timestamp = time();
	$datum = date("d.m.Y - H:i", $timestamp);
	echo $datum;
	?>
	<p id = "uhr"></p>

</div>
</div>
</header>
<body>

<!--Zur Seite Steuerungs_Modul-->
<div id="column1">
<a href="Steuerungsmodul.php">
Steuerungsmodul
</a>
</div><br/>

<!--Zur Seite Einstellungen-->
<div id="column2">
<a href="Einstellungen.php">
Einstellungen
</a>
</div><br/>

<hr>

<?php

// Einbinden der Konfigurationsdatei für mysql
require_once('/var/www/Datenbankverbindung.php');

// 5 Log Zeiten und Datum Anzeigen
$abfrage = "select *From log order by Datum Desc,Uhrzeit DESC;";
//überprügen ob abfrage korrekt
if(!$result = $db_link->query($abfrage))
{
echo 'fehler';
}
?>

<p>
Letzte Klingelzeiten:
</p>

<?php
// Ausgabe Log Dateien
$zaehler = 0;
// speichern aller logs in einem Array
while($row = $result->fetch_assoc())
{
	$Datum[$zaehler] = $row['Datum'];
	$Uhrzeit[$zaehler] = $row['Uhrzeit'];
	$Aktion[$zaehler] = $row['Aktion'];
	//Zählt die Einträge in der Datenbank
	$zaehler += 1;
}

// initialisieren des Zaehlers i
$i = 0;

//Auf der Weboberfläche werden Datum, Uhrzeit und Aktion(geöffnet) ausgegeben.
while($i<$zaehler and $i<5)
{
	echo "$Datum[$i] <br/>";
	echo "$Uhrzeit[$i] <br/>";
	if ($Aktion[$i] == 1)
		{
			echo "geöffnet <br/>";
		}
	echo "<br/>";
	$i+=1;
}

//gibt speicher frei
$result->free();

?>


</hr>

<!--Zur Seite Impressum-->
<div id="impressum">
<a href="Impressum.php">
Impressum
</a>
</div>
<br/>

<!--Website verlassen-->
<div id="Log_Out">
<a href="logout.php">
Log_Out
</a>
</div>

</body>
</html>
