<?php require_once('geheim.php');?>
<!DOCTYPE html>
<html lang ="de">
<head>
	<!--Einbinden von Bootstrap-->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<!--Einbinden von Uhrzeit.js um aktuelle Uhrzeit auszugeben-->
<script src = "Uhrzeit.js"></script>

<title>Email</title>

<header>
<h2>Türklingel V1 Team 24 / Emailadressen</h2>
<?php
//Aktuelles Datum und Uhrzeit anzeigen
$timestamp = time();
$datum = date("d.m.Y", $timestamp);
echo $datum;
?>
<p id = "uhr"></p>
</div>
</div>
</header>
</head>

<body>
<?php
// Einbinden der Konfigurationsdatei für mysql
require_once('/var/www/Datenbankverbindung.php')
?>

<!--Sprung zurück zu Steuerungs_Modul-->
<div id="column1">
<a href="Steuerungsmodul.php">
Steuerungs_Modul
</a>
</div></br>

<p>
<form method = "POST">
Neue Name einfügen:
<input type = "text" name= "Name">
<br/>
Neue Email-Adresse einfügen:
<input type = "email" name = "EmailAdresse">
<button>
speichern
</button>
</br>
Vorhandene Email-Adresse löschen:
<input type = "email" name = "löschenEmailAdresse">
<button>
löschen
</button>
</form>
</p>
<p>
<?php

//Wandelt entfernt Symbole: >< aus der Eingabe neue Name und
//speichert sie in neuer Variable
$neuername =filter_var($_POST['Name'],FILTER_SANITIZE_STRIPPED);
//Speichert die Eingabe neue Email in neuer Variable
$neueEmail = $_POST['EmailAdresse'];
//Speichert die Eingabe löschen in neuer Variable
$loeschenEmail = $_POST['löschenEmailAdresse'];

//Abfrage ob in beiden Textfelder etwas steht
if(!empty($neuername) and !empty($neueEmail))
{
	$Datenbankeintrag = "INSERT INTO EmailAdressen (Name,EmailAdressen) Values ('$neuername', '$neueEmail')";
	$Datenbankeintrag = mysqli_query($db_link,$Datenbankeintrag);
}

//Abfrage ob eingabe erfolgt ist
//schickt anfrage an datenbank
if(!empty($loeschenEmail))
{

	$Datenbankloeschen = "Delete from EmailAdressen where EmailAdressen = '$loeschenEmail'";
	$Datenbankloeschen = mysqli_query($db_link,$Datenbankloeschen);
}




// Alle EmailAdressen und Namen Anzeigen
$abfrage = "select * from EmailAdressen";
//überprügen ob abfrage korrekt
if(!$result = $db_link->query($abfrage))
{
echo 'fehler';
}

$zaehler = 0;
// speichern aller EmailAdressen und Namen in einem Array
while($row = $result->fetch_assoc())
{
	$Namen[$zaehler] = $row['Name'];
	$EmailAdressen[$zaehler] = $row['EmailAdressen'];
	//Zählt die Einträge in der Datenbank
	$zaehler += 1;
}

//gibt den Inahlt der Datenbank aus
for($i=0; $i<=$zaehler ; $i+=1)
{
	echo "$Namen[$i] <br/>";
	echo "$EmailAdressen[$i] <br/>";
	echo "<br/>";
}

?>

</p>

<?php
//gibt speicher frei
$result->free();
?>

<!--Zur Seite Impressum-->
<div id="impressum">
<a href="Impressum.php">
Impressum
</a>
</div>
<br/>

<!-- Ausloggen -->
<div id="Log_Out">
<a href="logout.php">
Log_Out
</a>
</div>
</body>
</html>
