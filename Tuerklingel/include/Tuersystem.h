/*
	Dateiname: Tuersystem.h
	Autor: Fabian Müller
	Änderungsdatum: 18.01.2018

	Zugriff auf die Klasse Weboberfläche
*/

#include "Weboberflaeche.h"
#ifndef TUERSYSTEM_H
#define TUERSYSTEM_H


class Tuersystem
{
    public:
        Tuersystem(); // Konstruktor (unbenutzt)
        virtual ~Tuersystem(); // Destruktor (unbenutzt)
        void klingeln(mysqlpp::Connection datenbank) const; // Betätigung der Klingel je nach Systemzustand
        void klingelton(mysqlpp::Connection datenbank) const; // Bei manuellem Systemzustand: Abspielen des Klingeltons und Abfrage über die Weboberfläche, ob die Tür geöffnet werden soll
        void oeffnen() const; // Öffnen der Tür
        void eMail() const; // Senden einer E-Mail an eine festgelegte Personengruppe

    protected:

    private:
        Weboberflaeche weboberflaeche; // Instanz der Klasse Weboberfläche
};

#endif // TUERSYSTEM_H
