/*
	Dateiname: Besucher.h
	Autor: Fabian Müller
	Änderungsdatum: 18.01.2018

	Zugriff auf die Klasse Türsystem
*/

#include "Tuersystem.h"
#ifndef BESUCHER_H
#define BESUCHER_H


class Besucher
{
    public:
        Besucher(); // Konstruktor (unbenutzt)
        virtual ~Besucher(); // Destruktor (unbenutzt)
        void klingeln(mysqlpp::Connection datenbank) const; // Betätigung der Klingel

    protected:

    private:
        Tuersystem tuersystem; // Instanz der Klasse Türsystem
};

#endif // BESUCHER_H
