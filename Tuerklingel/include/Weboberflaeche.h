/*
	Dateiname: Weboberflaeche.h
	Autor: Fabian Müller
	Änderungsdatum: 18.01.2018

	Zugriff auf die Klasse Benutzer
*/

#include "Benutzer.h"
#ifndef WEBOBERFLAECHE_H
#define WEBOBERFLAECHE_H


class Weboberflaeche
{
    public:
        Weboberflaeche(); // Konstruktor (unbenutzt)
        virtual ~Weboberflaeche(); // Destruktor (unbenutzt)
        bool anfrage(mysqlpp::Connection datenbank) const; // Anfrage, ob die Tür geöffnet werden soll
        void zeit() const; // Anlegen eines aktuellen Zeitstempels in der Logdatei
        void logeintrag(int log) const; // Anlegen eines Logeintrags je nach Nummer (Voraussetzung des Parameters "int log": ganze Zahl zwischen 0 und 2)
        void audio(mysqlpp::Connection datenbank) const; // Abspielen der Begrüßungsansage
        static int zustandAuslesen(mysqlpp::Connection datenbank); // Auslesen des Systemzustands aus der Datenbank
        static int getZustand(); // Ausgabe des Systemzustands
        static void setZustand(int zustand); // Programminternes Setzen des Systemzustandes (Voraussetzung des Parameters "int zustand": ganze Zahl zwischen 0 und 2)
        Benutzer benutzer; // Instanz der Klasse Benutzer

    protected:

    private:
        static int systemzustand; // Programminterne Speicherung des Systemzustandes
};

#endif // WEBOBERFLAECHE_H
