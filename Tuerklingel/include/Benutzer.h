/*
	Dateiname: Benutzer.h
	Autor: Fabian Müller
	Änderungsdatum: 18.01.2018

	Zugriff auf die von C++ bereitgestellten Klassen:
	<fstream> (Datenstrom - Schreiben in und Lesen aus einer Datei);
	<time.h> (Zeit - Zugriff auf die aktuelle Systemzeit);
	<iostream> (Ein- und Ausgaben in der Kommandozeile);
	<string> (Fließtext - Umgang mit dem Datentyp "string");
	<sstream> (Textstrom - Zwischenstufe der Konvertierung von Fließtext in ganze Zahlen)
	<mysqlpp.h> (Verknüpfung mit der SQL-Datenbank
    <unistd.h> (benötigt für die Zeitverzögerung)
	systemweite Verwendung des Namensraums "std::[...]"
*/

#include <fstream>
#include <time.h>
#include <iostream>
#include <string>
#include <sstream>
#include <mysql++.h>
#include <unistd.h>
using namespace std;
#ifndef BENUTZER_H
#define BENUTZER_H


class Benutzer
{
    public:
        Benutzer(); // Konstruktor (unbenutzt)
        virtual ~Benutzer(); // Destruktor (unbenutzt)
        bool anfrage(mysqlpp::Connection datenbank) const; // Anfrage, ob die Tür geöffnet werden soll
        string datum() const; // Ausgeben des aktuellen Datums als Text
        string zeit() const; // Ausgeben der aktuellen Zeit als Text
        void zeitSQL(mysqlpp::Connection datenbank, char aktion) const; // aktueller Zeitstempel in SQL (nicht geöffnet: 0; geöffnet: 1)
        string intToString(int i) const; // Konverter einer Zahl in einen Text

    protected:

    private:

};

#endif // BENUTZER_H
