/*
	Dateiname: Benutzer.cpp
	Autor: Fabian Müller
	Änderungsdatum: 18.01.2018

	Dieses Programm steuert die gesamte Anlage der Türklingel.
	Es fragt den aktuellen Systemzustand (aus [0], automatisch [1] oder manuell [2] ab,
	gibt ihn aus und ermöglicht Besuchern, die Klingel zu betätigen.
*/

#include "Benutzer.h"

Benutzer::Benutzer() // Konstruktor (unbenutzt)
{

}

Benutzer::~Benutzer() // Destruktor (unbenutzt)
{

}

bool Benutzer::anfrage(mysqlpp::Connection datenbank) const // Anfrage, ob die Tür geöffnet werden soll
{
    mysqlpp::Query abfrage = datenbank.query("UPDATE Tuerverhalten SET Nummer = 1"); // Weiterleitung der Anfrage über SQL an die Weboberfläche (Nummer 1)
    mysqlpp::SimpleResult aenderung = abfrage.execute();
    int t = 0;
    while(1) // dauerhafte Überwachung der Benutzerantwort
    {
        abfrage = datenbank.query("SELECT t.Nummer FROM Tuerverhalten t"); // Auslesen der Benutzerantwort (1: noch keine Antwort; 2: Bestätigung der Türöffnung; 0: Ablehnung der Türöffnung)
        mysqlpp::StoreQueryResult ablage = abfrage.store(); // Ablegen der Benutzerantwort im Zwischenspeicher
        if(!ablage) // Prüfen, ob beim Auslesen der Benutzerantwort ein Fehler aufgetreten ist
        {
            cout << "Die Benutzerantwort konnte nicht ausgelesen werden und die Türöffnung wird abgelehnt." << endl; // Fehlermeldung
            return 0; // Ablehnung der Anfrage zur Türöffnung
        }
        else if(t>299) // bei fehlerfreiem Auslesen und keiner Reaktion des Benutzers für 300*0,1s = 30s
        {
            abfrage = datenbank.query("UPDATE Tuerverhalten SET Nummer = 0"); // Rücksetzen der Benutzerantwort auf 0
            aenderung = abfrage.execute();
            zeitSQL(datenbank, '0'); // Anlegen eines Zeitstempels in SQL (keine Türöffnung)
            return 0; // Ablehnung der Anfrage zur Türöffung
        }
        else if(ablage[0]["Nummer"] == "2") // bei Bestätigung durch den Benutzer
        {
            abfrage = datenbank.query("UPDATE Tuerverhalten SET Nummer = 0"); // Rücksetzen der Benutzerantwort auf 0
            aenderung = abfrage.execute();
            zeitSQL(datenbank, '1'); // Anlegen eines Zeitstempels in SQL (Türöffnung)
            return 1; // Bestätigung der Anfrage zur Türöffnung
        }
        unsigned int mys = 100000; // Erstellen der Variable für 0,1s
        usleep(mys); // Warten für 0,1s
        t++;
    }
}

string Benutzer::datum() const // Ausgeben des aktuellen Datums als Text
{
    string datum; // Anlegen des Datums als Text
    time_t zeit1; // Anlegen des Zeitobjekts
    tm *jetzt; // Anlegen der Zeitstruktur
    zeit1 = time(0); // Setzen der Aktuellen Zeit
    jetzt = localtime(&zeit1); // Übertragung der aktuellen Zeit in die Struktur (Aufteilung in Jahr, Monat, Tag, Stunde, Minute und Sekunde)
    int jahr = jetzt->tm_year + 1900;
    datum += intToString(jahr); // aktuelles Jahr
    datum += '-'; // Strich zwischen Jahr und Monat
    if(jetzt->tm_mon < 9) // Abfrage, ob der aktuelle Monat einstellig ist
        datum += '0'; // gegebenenfalls Auffüllen mit einer Null
    int monat = jetzt->tm_mon + 1;
    datum += intToString(monat); // aktueller Monat
    datum += '-'; // Strich zwischen Monat und Tag
    if(jetzt->tm_mday < 10) // Abfrage, ob der aktuelle Tag einstellig ist
        datum += '0'; // gegebenenfalls Auffüllen mit einer Null
    int tag = jetzt->tm_mday;
    datum += intToString(tag); // aktueller Tag
    return datum;
}

string Benutzer::zeit() const // Ausgeben der aktuellen Zeit als Text
{
    string zeit; // Anlegen der Zeit als Text
    time_t zeit1; // Anlegen des Zeitobjekts
    tm *jetzt; // Anlegen der Zeitstruktur
    zeit1 = time(0); // Setzen der Aktuellen Zeit
    jetzt = localtime(&zeit1); // Übertragung der aktuellen Zeit in die Struktur (Aufteilung in Jahr, Monat, Tag, Stunde, Minute und Sekunde)
    if(jetzt->tm_hour < 10) // Abfrage, ob die aktuelle Stunde einstellig ist
        zeit += '0'; // gegebenenfalls Auffüllen mit einer Null
    int stunde = jetzt->tm_hour;
    zeit += intToString(stunde); // aktuelle Stunde
    zeit += ':'; // Doppelpunkt zwischen Stunde und Minute
    if(jetzt->tm_min < 10) // Abfrage, ob die aktuelle Minute einstellig ist
        zeit += '0'; // gegebenenfalls Auffüllen mit einer Null
    int minute = jetzt->tm_min;
    zeit += intToString(minute); // aktuelle Minute
    zeit += ':'; // Doppelpunkt zwischen Minute und Sekunde
    if(jetzt->tm_sec < 10) // Abfrage, ob die aktuelle Sekunde einstellig ist
        zeit += '0'; // gegebenenfalls Auffüllen mit einer Null
    int sekunde = jetzt->tm_sec;
    zeit += intToString(sekunde); // aktuelle Sekunde
    return zeit;
}

void Benutzer::zeitSQL(mysqlpp::Connection datenbank, char aktion) const // aktueller Zeitstempel in SQL (nicht geöffnet: 0; geöffnet: 1)
{
    string befehl = "INSERT INTO log (Datum,Uhrzeit,Aktion) VALUES (\"" + datum() + "\",\"" + zeit() + "\",\"" + aktion + "\")"; // Setzen des aktuellen Datums, der aktuellen Zeit und der Aktion 0 (Ablehnung der Anfrage) in die Log-Datenbank
    mysqlpp::Query abfrage = datenbank.query(befehl); // Ausführen des Befehls
    mysqlpp::SimpleResult aenderung = abfrage.execute();
}

string Benutzer::intToString(int i) const // Konverter einer Zahl in einen Text
{
    string t; // Anlegen eines Textes
    stringstream converter; // Anlegen eines Textstroms
    converter.clear(); // Leeren des Textstroms
    converter << i; // Aufnahme der Zahl in den Textstrom
    converter >> t; // Schreiben des Textstroms in den Text
    return t; // Ausgabe der Zahl als Text
}
