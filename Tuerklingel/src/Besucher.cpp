/*
	Dateiname: Besucher.cpp
	Autor: Fabian Müller
	Änderungsdatum: 18.01.2018

	Diese Klasse leitet die Betätigung der Klingel durch den Besucher an das Türsystem weiter.
*/

#include "Besucher.h"

Besucher::Besucher() // Konstruktor (unbenutzt)
{

}

Besucher::~Besucher() // Destruktor (unbenutzt)
{

}

void Besucher::klingeln(mysqlpp::Connection datenbank) const // Betätigung der Klingel
{
    tuersystem.klingeln(datenbank); // Zugriff auf die Klingel im Türsystem
}
