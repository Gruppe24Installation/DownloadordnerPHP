/*
	Dateiname: Weboberflaeche.cpp
	Autor: Fabian Müller
	Änderungsdatum: 18.01.2018

	Diese Klasse beinhaltet den Systemzustand, den sie aus einer Konfigurationsdatei auslesen, intern setzen und intern abfragen kann.
	Außerdem schreibt sie drei verschiedene Logeinträge mit Zeitstempel in die Logdatei
	und leitet Anfragen des Türsystems, ob die Tür manuell geöffnet werden soll oder nicht, an den Benutzer weiter.
*/

#include "Weboberflaeche.h"

int Weboberflaeche::systemzustand = 0; // Inizialisierung des Systemzustandes mit Wert 0 (aus)

Weboberflaeche::Weboberflaeche() // Konstruktor (unbenutzt)
{

}

Weboberflaeche::~Weboberflaeche() // Destruktor (unbenutzt)
{

}

bool Weboberflaeche::anfrage(mysqlpp::Connection datenbank) const // Anfrage, ob die Tür geöffnet werden soll
{
    return benutzer.anfrage(datenbank); // Weiterleitung der Anfrage an den Benutzer
}

void Weboberflaeche::zeit() const // Anlegen eines aktuellen Zeitstempels in der Logdatei
{
    fstream f; // Anlegen eines Datenstroms
    f.open("log.txt", ios::app); // Öffnen der Logdatei
    time_t zeit; // Anlegen des Zeitobjekts
    tm *jetzt; // Anlegen der Zeitstruktur
    zeit = time(0); // Setzen der Aktuellen Zeit
    jetzt = localtime(&zeit); // Übertragung der aktuellen Zeit in die Struktur (Aufteilung in Jahr, Monat, Tag, Stunde, Minute und Sekunde)
    if(jetzt->tm_mday < 10) // Abfrage, ob der aktuelle Tag einstellig ist
        f << 0; // gegebenenfalls Auffüllen mit einer Null
    f << jetzt->tm_mday << '.'; // Punkt für den Tag
    if(jetzt->tm_mon < 9) // Abfrage, ob der aktuelle Monat einstellig ist
        f << 0; // gegebenenfalls Auffüllen mit einer Null
    f << jetzt->tm_mon + 1 << '.'; // Punkt für den Monat
    f << jetzt->tm_year + 1900 << " - "; // Aktuelles Jahr und Bindestrich als Abtrennung zur Uhrzeit
    if(jetzt->tm_hour < 10) // Abfrage, ob die aktuelle Stunde einstellig ist
        f << 0; // gegebenenfalls Auffüllen mit einer Null
    f << jetzt->tm_hour << ':'; // Doppelpunkt zwischen Stunde und Minute
    if(jetzt->tm_min < 10) // Abfrage, ob die aktuelle Minute einstellig ist
        f << 0; // gegebenenfalls Auffüllen mit einer Null
    f << jetzt->tm_min << ':'; // Doppelpunkt zwischen Minute und Sekunde
    if(jetzt->tm_sec < 10) // Abfrage, ob die aktuelle Sekunde einstellig ist
        f << 0; // gegebenenfalls Auffüllen mit einer Null
    f << jetzt->tm_sec << ": "; // Doppelpunkt nach dem Zeitstempel und vor dem Logeintrag
    f.close(); // Schließen des Datenstroms
}

void Weboberflaeche::logeintrag(int log) const // Anlegen eines Logeintrags je nach Nummer (Voraussetzung des Parameters "int log": ganze Zahl zwischen 0 und 2)
{
    zeit(); // Anlegen eines aktuellen Zeitstempels in der Logdatei
    fstream f; // Anlegen eines Datenstroms
    f.open("log.txt", ios::app); // Öffnen der Logdatei
    switch(log) // Abfrage, welcher Logeintrag geschrieben werden soll
    {
    case 0: // Schreiben des Logeintrags Nummer 0 in die Logdatei
        f << "Betätigung der Türklingel";
        break;
    case 1: // Schreiben des Logeintrags Nummer 1 in die Logdatei
        f << "Automatisches Öffnen der Tür";
        break;
    case 2: // Schreiben des Logeintrags Nummer 2 in die Logdatei
        f << "Manuelles Öffnen der Tür";
        break;
    case 3: // Schreiben des Logeintrags Nummer 3 in die Logdatei
        f << "Ablehnung der manuellen Türöffnung";
        break;
    }
    f << "<br/>" << endl; // Einfügen des Zeilenumbruchs für die Anzeige in HTML
    f.close(); // Schließen des Datenstroms
}

void Weboberflaeche::audio(mysqlpp::Connection datenbank) const // Abspielen der Begrüßungsansage über die Weboberfläche
{
    mysqlpp::Query abfrage = datenbank.query("UPDATE Audio SET Status = 1"); // Weiterleitung des Audioaufrufs über SQL an die Weboberfläche (Status 1)
    mysqlpp::SimpleResult aenderung = abfrage.execute();
}

int Weboberflaeche::zustandAuslesen(mysqlpp::Connection datenbank) // Auslesen des Systemzustands aus der Datenbank
{
    mysqlpp::Query abfrage = datenbank.query("SELECT s.Zustand FROM Systemzustand s"); // Abfrage des Zustandes aus der Datenbank
    mysqlpp::StoreQueryResult ablage = abfrage.store(); // Ablegen der Zustandsabfrage im Zwischenspeicher
    if(!ablage) // Prüfen, ob beim Auslesen des Systemzustandes ein Fehler aufgetreten ist
    {
        cout << "Der Systemzustand konnte nicht ausgelesen werden und wird auf 0 gesetzt." << endl; // Fehlermeldung
        return 0; // Ausgabe des Systemzustandes als 0 (aus)
    } else // bei fehlerfreiem Auslesen
        return ablage[0]["Zustand"]; // Ausgabe des ersten Wertes in der Tabelle Zustand als Systemzustand
}

int Weboberflaeche::getZustand() // Ausgabe des Systemzustands
{
    return systemzustand;
}

void Weboberflaeche::setZustand(int zustand) // Programminternes Setzen des Systemzustandes (Voraussetzung des Parameters "int zustand": ganze Zahl zwischen 0 und 2)
{
    systemzustand = zustand;
}
