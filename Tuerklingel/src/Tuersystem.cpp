/*
	Dateiname: Tuersystem.cpp
	Autor: Fabian Müller
	Änderungsdatum: 18.01.2018

	Diese Klasse fragt den Systemzustand ab und öffnet die Tür nicht [0], automatisch [1] oder manuell [0] und greift dabei auch auf die Klasse Weboberfläche zu.
*/

#include "Tuersystem.h"

Tuersystem::Tuersystem() // Konstruktor (unbenutzt)
{

}

Tuersystem::~Tuersystem() // Destruktor (unbenutzt)
{

}

void Tuersystem::klingeln(mysqlpp::Connection datenbank) const // Betätigung der Klingel je nach Systemzustand
{
    weboberflaeche.logeintrag(0); // Logeintrag Nummer 0: "Betätigung der Türklingel"
    mysqlpp::Query abfrage = datenbank.query(); // Erstellen einer SQL-Abfrage
    mysqlpp::SimpleResult aenderung = abfrage.execute(); // Erstellen einer SQL-Bearbeitung
    unsigned int mys = 1000000; // Erstellen der Variable für 1s
    switch(weboberflaeche.getZustand()) // Abfrage des Systemzustandes
    {
    case 0: // Systemzustand 0: aus
        weboberflaeche.benutzer.zeitSQL(datenbank, '0'); // Anlegen eines Zeitstempels in SQL (keine Türöffnung)
        break;
    case 1: // Systemzustand 1: automatisch
        oeffnen(); // automatisches Öffnen der Tür
        weboberflaeche.logeintrag(1); // Logeintrag Nummer 1: "Automatisches Öffnen der Tür"
        usleep(mys); // Warten für 1s
        weboberflaeche.audio(datenbank); // Abspielen der Begrüßungsansage über die Weboberfläche
        abfrage = datenbank.query("UPDATE Benachrichtigung SET Status = 1"); // Benachrichtigen des Benutzers über die Weboberfläche
        aenderung = abfrage.execute();
        weboberflaeche.benutzer.zeitSQL(datenbank, '1'); // Anlegen eines Zeitstempels in SQL (Türöffnung)
        break;
    case 2: // Systemzustand 2: manuell
        eMail(); // Senden einer E-Mail an eine festgelegte Personengruppe
        klingelton(datenbank); // Abspielen des Klingeltons und Abfrage über die Weboberfläche, ob die Tür geöffnet werden soll
        break;
    }
}

void Tuersystem::klingelton(mysqlpp::Connection datenbank) const // Bei manuellem Systemzustand: Abspielen des Klingeltons und Abfrage über die Weboberfläche, ob die Tür geöffnet werden soll
{
    if(weboberflaeche.anfrage(datenbank)) // Benutzerabfrage über die Weboberfläche, ob die Tür geöffnet werden soll (Rückgabe: 0 (nein); 1 (ja))
    {
        oeffnen(); // Öffnen der Tür
        weboberflaeche.logeintrag(2); // Logeintrag Nummer 2: "Manuelles Öffnen der Tür"
    }
    else
        weboberflaeche.logeintrag(3); // Logeintrag Nummer 3: "Ablehnung der manuellen Türöffnung"
}

void Tuersystem::oeffnen() const // Öffnen der Tür
{
    cout << "Die Tür kann geöffnet werden." << endl; // Simulation der Türöffnung
}

void Tuersystem::eMail() const // Senden einer E-Mail an eine festgelegte Personengruppe
{
    cout << "Die Benutzer werden per E-Mail über das Klingeln informiert." << endl;
}
